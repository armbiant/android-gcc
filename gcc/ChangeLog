2025-01-12  Maciej W. Rozycki  <macro@orcam.me.uk>

	* config/alpha/alpha.cc (alpha_expand_block_move): Use a HImode
	subreg of a DImode register to hold data from an aligned HImode
	load.

2025-01-12  Maciej W. Rozycki  <macro@orcam.me.uk>

	* config/alpha/alpha.cc (alpha_expand_block_move): Merge loaded
	data from pairs of SImode registers into single DImode registers
	if to be used with unaligned stores.

2025-01-12  Maciej W. Rozycki  <macro@orcam.me.uk>

	* config/alpha/alpha.cc (alpha_option_override): Ignore CPU
	flags corresponding to features the enabling or disabling of
	which has been requested with an individual feature option.

2025-01-12  Maciej W. Rozycki  <macro@orcam.me.uk>

	PR middle-end/64242
	* config/alpha/alpha.md (`builtin_longjmp'): Restore frame
	pointer last.  Add frame clobber and schedule blockage.

2025-01-12  Maciej W. Rozycki  <macro@orcam.me.uk>

	* config/alpha/alpha.md (builtin_longjmp): Add memory clobbers.

2025-01-12  Richard Biener  <rguenther@suse.de>

	* tree-vect-slp.cc (vect_analyze_slp): Release saved_stmts
	vector.
	(vect_build_slp_tree_2): Release new_oprnds_info when not
	used.
	(vect_analyze_slp): Release root_stmts when gcond SLP
	build fails.

2025-01-12  Andrew Pinski  <quic_apinski@quicinc.com>

	PR middle-end/118411
	* final.cc (get_attr_length_1): Handle asm for CALL_INSN
	and JUMP_INSNs.

2025-01-11  mengqinggang  <mengqinggang@loongson.cn>

	* config/loongarch/lasx.md: Use new loongarch_output_move.
	* config/loongarch/loongarch-protos.h (loongarch_output_move):
	Change parameters from (rtx, rtx) to (rtx *).
	* config/loongarch/loongarch.cc (loongarch_output_move):
	Generate final immediate for lu12i.w and lu52i.d.
	* config/loongarch/loongarch.md:
	Generate final immediate for lu32i.d and lu52i.d.
	* config/loongarch/lsx.md: Use new loongarch_output_move.

2025-01-11  Andrew MacLeod  <amacleod@redhat.com>

	PR tree-optimization/88575
	* vr-values.cc (simplify_using_ranges::fold_cond_with_ops): Query
	relation between op0 and op1 and utilize it.
	(simplify_using_ranges::simplify): Do not eliminate float checks.

2025-01-10  Alex Coplan  <alex.coplan@arm.com>

	PR tree-optimization/118211
	PR tree-optimization/116126
	* tree-vect-loop.cc (vect_compute_single_scalar_iteration_cost):
	Don't skip over gconds.

2025-01-10  Alex Coplan  <alex.coplan@arm.com>

	PR tree-optimization/118211
	PR tree-optimization/116126
	* tree-vect-loop-manip.cc (vect_do_peeling): Adjust skip_vector
	condition to only omit the edge if we're versioning for
	alignment.

2025-01-10  Tamar Christina  <Tamar.Christina@arm.com>
	    Alex Coplan  <alex.coplan@arm.com>

	PR tree-optimization/118211
	PR tree-optimization/116126
	* tree-vect-loop-manip.cc (vect_do_peeling): Update immediate
	dominators of nodes that were dominated by the prolog skip block
	after inserting vector skip edge.  Initialize prolog variable to
	NULL to avoid bogus -Wmaybe-uninitialized during bootstrap.

2025-01-10  Alex Coplan  <alex.coplan@arm.com>

	PR tree-optimization/118211
	PR tree-optimization/116126
	* tree-vect-loop-manip.cc (vect_do_peeling): Avoid emitting an
	epilogue guard for inverted early-exit loops.

2025-01-10  Alex Coplan  <alex.coplan@arm.com>
	    Tamar Christina  <tamar.christina@arm.com>

	PR tree-optimization/118211
	PR tree-optimization/116126
	* tree-vect-data-refs.cc (vect_analyze_early_break_dependences):
	Set need_peeling_for_alignment flag on read DRs instead of
	failing vectorization.  Punt on gathers.
	(dr_misalignment): Handle non-constant target alignments.
	(vect_compute_data_ref_alignment): If need_peeling_for_alignment
	flag is set on the DR, then override the target alignment chosen
	by the preferred_vector_alignment hook to choose a safe
	alignment.
	(vect_supportable_dr_alignment): Override
	support_vector_misalignment hook if need_peeling_for_alignment
	is set on the DR: in this case we must return
	dr_unaligned_unsupported in order to force peeling.
	* tree-vect-loop-manip.cc (vect_do_peeling): Allow prolog
	peeling by a compile-time non-constant amount.
	* tree-vectorizer.h (dr_vec_info): Add new flag
	need_peeling_for_alignment.

2025-01-10  Tamar Christina  <tamar.christina@arm.com>

	* config/aarch64/aarch64-cores.def (AARCH64_CORE): Fix cortex-x4 parts
	num.

2025-01-10  Richard Biener  <rguenther@suse.de>

	* df-core.cc (rest_of_handle_df_finish): Release dflow for
	problems without free function (like LR).
	* gimple-crc-optimization.cc (crc_optimization::loop_may_calculate_crc):
	Release loop_bbs on all exits.
	* tree-vectorizer.h (supportable_indirect_convert_operation): Change.
	* tree-vect-generic.cc (expand_vector_conversion): Adjust.
	* tree-vect-stmts.cc (vectorizable_conversion): Use auto_vec for
	converts.
	(supportable_indirect_convert_operation): Get a reference to
	the output vector of converts.

2025-01-10  Christophe Lyon  <christophe.lyon@linaro.org>

	PR target/118332
	* config/arm/arm-mve-builtins.cc (wrap_type_in_struct): Delete.
	(register_type_decl): Delete.
	(register_builtin_tuple_types): Use
	lang_hooks.types.simulate_record_decl.

2025-01-10  Richard Biener  <rguenther@suse.de>

	* gcse.cc (pass_hardreg_pre::gate): Wrap possibly unused
	fun argument.

2025-01-10  Richard Biener  <rguenther@suse.de>

	PR rtl-optimization/117467
	PR rtl-optimization/117934
	* ext-dce.cc (ext_dce_execute): Do nothing if a memory
	allocation estimate exceeds what is allowed by
	--param max-gcse-memory.

2025-01-10  Stefan Schulze Frielinghaus  <stefansf@gcc.gnu.org>

	* config/s390/s390-protos.h (s390_emit_compare): Add mode
	parameter for the resulting RTX.
	* config/s390/s390.cc (s390_emit_compare): Dito.
	(s390_emit_compare_and_swap): Change.
	(s390_expand_vec_strlen): Change.
	(s390_expand_cs_hqi): Change.
	(s390_expand_split_stack_prologue): Change.
	* config/s390/s390.md (*add<mode>3_carry1_cc): Renamed to ...
	(add<mode>3_carry1_cc): this and in order to use the
	corresponding gen function, encode CC mode into pattern.
	(*sub<mode>3_borrow_cc): Renamed to ...
	(sub<mode>3_borrow_cc): this and in order to use the
	corresponding gen function, encode CC mode into pattern.
	(*add<mode>3_alc_carry1_cc): Renamed to ...
	(add<mode>3_alc_carry1_cc): this and in order to use the
	corresponding gen function, encode CC mode into pattern.
	(sub<mode>3_slb_borrow1_cc): New.
	(uaddc<mode>5): New.
	(usubc<mode>5): New.

2025-01-10  Andrew Carlotti  <andrew.carlotti@arm.com>

	* doc/passes.texi: Document hardreg PRE pass.

2025-01-10  Andrew Carlotti  <andrew.carlotti@arm.com>

	* config/aarch64/aarch64.h (HARDREG_PRE_REGNOS): New macro.
	* gcse.cc (doing_hardreg_pre_p): New global variable.
	(do_load_motion): New boolean check.
	(current_hardreg_regno): New global variable.
	(compute_local_properties): Unset transp for hardreg clobbers.
	(prune_hardreg_uses): New function.
	(want_to_gcse_p): Use different checks for hardreg PRE.
	(oprs_unchanged_p): Disable load motion for hardreg PRE pass.
	(hash_scan_set): For hardreg PRE, skip non-hardreg sets and
	check for hardreg clobbers.
	(record_last_mem_set_info): Skip for hardreg PRE.
	(compute_pre_data): Prune hardreg uses from transp bitmap.
	(pre_expr_reaches_here_p_work): Add sentence to comment.
	(insert_insn_start_basic_block): New functions.
	(pre_edge_insert): Don't add hardreg sets to predecessor block.
	(pre_delete): Use hardreg for the reaching reg.
	(reset_hardreg_debug_uses): New function.
	(pre_gcse): For hardreg PRE, reset debug uses and don't insert
	copies.
	(one_pre_gcse_pass): Disable load motion for hardreg PRE.
	(execute_hardreg_pre): New.
	(class pass_hardreg_pre): New.
	(pass_hardreg_pre::gate): New.
	(make_pass_hardreg_pre): New.
	* passes.def (pass_hardreg_pre): New pass.
	* tree-pass.h (make_pass_hardreg_pre): New.

2025-01-10  Andrew Carlotti  <andrew.carlotti@arm.com>

	* multiple_target.cc
	(redirect_to_specific_clone): Assert that "target" attribute is
	used for FMV before checking it.
	(ipa_target_clone): Skip redirect_to_specific_clone on some
	targets.

2025-01-10  Andrew Carlotti  <andrew.carlotti@arm.com>

	* doc/invoke.texi: Add new AArch64 flags.

2025-01-10  Andrew Carlotti  <andrew.carlotti@arm.com>

	* config/aarch64/aarch64-arches.def (V8_7A): Add XS.
	* config/aarch64/aarch64-option-extensions.def (XS): New flag.

2025-01-10  Andrew Carlotti  <andrew.carlotti@arm.com>

	* config/aarch64/aarch64-arches.def (V8_7A): Add WFXT.
	* config/aarch64/aarch64-option-extensions.def (WFXT): New flag.

2025-01-10  Andrew Carlotti  <andrew.carlotti@arm.com>

	* config/aarch64/aarch64-arches.def (V8_4A): Add RCPC2.
	* config/aarch64/aarch64-option-extensions.def
	(RCPC2): New flag.
	(RCPC3): Add RCPC2 dependency.
	* config/aarch64/aarch64.h (TARGET_RCPC2): Use new flag.

2025-01-10  Andrew Carlotti  <andrew.carlotti@arm.com>

	* config/aarch64/aarch64-arches.def (V8_5A): Add FLAGM2.
	* config/aarch64/aarch64-option-extensions.def (FLAGM2): New flag.

2025-01-10  Andrew Carlotti  <andrew.carlotti@arm.com>

	* config/aarch64/aarch64-arches.def (V8_5A): Add FRINTTS
	* config/aarch64/aarch64-option-extensions.def (FRINTTS): New flag.
	* config/aarch64/aarch64.h (TARGET_FRINT): Use new flag.
	* config/aarch64/arm_acle.h: Use new flag for frintts intrinsics.
	* config/aarch64/arm_neon.h: Ditto.

2025-01-10  Andrew Carlotti  <andrew.carlotti@arm.com>

	* config/aarch64/aarch64-arches.def (V8_3A): Add JSCVT.
	* config/aarch64/aarch64-option-extensions.def (JSCVT): New flag.
	* config/aarch64/aarch64.h (TARGET_JSCVT): Use new flag.
	* config/aarch64/arm_acle.h: Use new flag for jscvt intrinsics.

2025-01-10  Andrew Carlotti  <andrew.carlotti@arm.com>

	* config/aarch64/aarch64-arches.def (V8_3A): Add FCMA.
	* config/aarch64/aarch64-option-extensions.def (FCMA): New flag.
	(SVE): Add FCMA dependency.
	* config/aarch64/aarch64.h (TARGET_COMPLEX): Use new flag.
	* config/aarch64/arm_neon.h: Use new flag for fcma intrinsics.

2025-01-10  Andrew Carlotti  <andrew.carlotti@arm.com>

	* config/aarch64/aarch64.cc
	(aarch64_expand_epilogue): Use TARGET_PAUTH.
	* config/aarch64/aarch64.md: Update comment.

2025-01-10  Richard Sandiford  <richard.sandiford@arm.com>

	PR rtl-optimization/117186
	* rtl.h (simplify_context::simplify_logical_relational_operation): Add
	an invert0_p parameter.
	* simplify-rtx.cc (unsigned_comparison_to_mask): New function.
	(mask_to_unsigned_comparison): Likewise.
	(comparison_code_valid_for_mode): Delete.
	(simplify_context::simplify_logical_relational_operation): Add
	an invert0_p parameter.  Handle AND and XOR.  Handle unsigned
	comparisons.  Handle always-false results.  Ignore the low bit
	of the mask if the operands are always ordered and remove the
	then-redundant check of comparison_code_valid_for_mode.  Check
	for side-effects in the operands before simplifying them away.
	(simplify_context::simplify_binary_operation_1): Remove
	simplification of (compare (gt ...) (lt ...)) and instead...
	(simplify_context::simplify_relational_operation_1): ...handle
	comparisons of comparisons here.
	(test_comparisons): New function.
	(test_scalar_ops): Call it.

2025-01-10  Alexandre Oliva  <oliva@adacore.com>

	* gimple-fold.cc (decode_field_reference): Drop misuses of
	uniform_integer_cst_p.
	(fold_truth_andor_for_ifcombine): Likewise.

2025-01-10  Alexandre Oliva  <oliva@adacore.com>

	PR tree-optimization/118344
	* gimple-fold.cc (fold_truth_andor_for_ifcombine): Fix typo in
	rr_and_mask's type adjustment test.

2025-01-10  Alexandre Oliva  <oliva@adacore.com>

	* gimple-fold.cc (decode_field_reference): Add xor_pand_mask.
	Propagate pand_mask to the right-hand xor operand.  Don't
	require the right-hand xor operand to be a constant.
	(fold_truth_andor_for_ifcombine): Pass right-hand mask when
	appropriate.

2025-01-10  Alexandre Oliva  <oliva@adacore.com>

	PR tree-optimization/118206
	* gimple-fold.cc (decode_field_reference): Account for upper
	bits dropped by narrowing conversions whether before or after
	a right shift.
	(fold_truth_andor_for_ifcombine): Fold masks, compares, and
	combined results.

2025-01-10  Alexandre Oliva  <oliva@adacore.com>

	* gimple-fold.cc (fold_truth_andor_for_ifcombine): Limit
	boundary choice by word size as well.  Try aligned double-word
	loads as a last resort.

2025-01-10  Martin Jambor  <mjambor@suse.cz>

	PR ipa/118138
	* ipa-cp.cc (ipacp_value_safe_for_type): Return the appropriate
	type instead of a bool, accept NULL_TREE VALUEs.
	(propagate_vals_across_arith_jfunc): Use the new returned value of
	ipacp_value_safe_for_type.
	(propagate_vals_across_ancestor): Likewise.
	(propagate_scalar_across_jump_function): Likewise.

2025-01-10  chenxiaolong  <chenxiaolong@loongson.cn>
	    Deng Jianbo  <dengjianbo@loongson.cn>.

	* config/loongarch/loongarch.cc
	(loongarch_builtin_vectorization_cost): Modify the
	construction cost of the vec_construct vector.

2025-01-09  Tamar Christina  <tamar.christina@arm.com>

	PR target/118188
	* config/aarch64/aarch64.cc (aarch64_vector_costs::count_ops): Adjust
	throughput of emulated gather and scatters.

2025-01-09  Vladimir N. Makarov  <vmakarov@redhat.com>

	PR target/118017
	* lra-constraints.cc (inherit_reload_reg): Check reg class on uniformity.

2025-01-09  Stefan Schulze Frielinghaus  <stefansf@gcc.gnu.org>

	PR target/118362
	* config/s390/s390.cc (s390_constant_via_vgbm_p): Allow at most
	16-byte vectors.

2025-01-09  Christophe Lyon  <christophe.lyon@linaro.org>

	PR target/118131
	* config/arm/arm.h (VALID_MVE_STRUCT_MODE): Accept TI, OI and XI
	modes again.

2025-01-09  Thomas Schwinge  <tschwinge@baylibre.com>

	PR target/65181
	* config/nvptx/nvptx.cc (nvptx_get_drap_rtx): Handle
	'!TARGET_SOFT_STACK'.
	* config/nvptx/nvptx.md (define_c_enum "unspec"): Add
	'UNSPEC_STACKSAVE', 'UNSPEC_STACKRESTORE'.
	(define_expand "allocate_stack", define_expand "save_stack_block")
	(define_expand "save_stack_block"): Handle '!TARGET_SOFT_STACK',
	PTX 'alloca'.
	(define_insn "@nvptx_alloca_<mode>")
	(define_insn "@nvptx_stacksave_<mode>")
	(define_insn "@nvptx_stackrestore_<mode>"): New.
	* doc/invoke.texi (Nvidia PTX Options): Update '-msoft-stack',
	'-mno-soft-stack'.
	* doc/sourcebuild.texi (nvptx-specific attributes): Document
	'nvptx_runtime_alloca_ptx'.
	(Add Options): Document 'nvptx_alloca_ptx'.

2025-01-09  Richard Biener  <rguenther@suse.de>

	* cfgloopmanip.cc (duplicate_loop_body_to_header_edge): When
	copying to the header edge first redirect the entry to the
	new loop and then the exit to the old to avoid PHI node
	re-allocation.

2025-01-09  H.J. Lu  <hjl.tools@gmail.com>

	PR rtl-optimization/118266
	* ree.cc (add_removable_extension): Skip extension on fixed
	register.

2025-01-09  Jakub Jelinek  <jakub@redhat.com>
	    Andrew Pinski  <quic_apinski@quicinc.com>

	PR tree-optimization/117927
	* tree-pass.h (PROP_last_full_fold): Define.
	* passes.def: Add last= parameters to pass_forwprop.
	* tree-ssa-forwprop.cc (pass_forwprop): Add last_p non-static
	data member and initialize it in the ctor.
	(pass_forwprop::set_pass_param): New method.
	(pass_forwprop::execute): Set PROP_last_full_fold in curr_properties
	at the start if last_p.
	* match.pd (a rrotate (32-b) -> a lrotate b): Only optimize either
	if @2 is known not to be equal to prec or if during/after last
	forwprop the subtraction has single use and prec is power of two; in
	that case transform it into orotate by masked count.

2025-01-09  Haochen Jiang  <haochen.jiang@intel.com>

	* common/config/i386/cpuinfo.h (get_intel_cpu): Remove 0x00.

2025-01-09  xuli  <xuli1@eswincomputing.com>

	* config/riscv/riscv-vector-builtins.cc (function_builder::add_unique_function):
	Only register overloaded intrinsic for g++.
	Only insert non_overloaded_function_table for gcc.
	(function_builder::add_overloaded_function): Only register overloaded intrinsic for gcc.
	(handle_pragma_vector): Only initialize non_overloaded_function_table for gcc.

2025-01-09  Tobias Burnus  <tburnus@baylibre.com>

	* builtin-types.def (BT_FN_PTRMODE_PTR_INT_PTR): Add.
	* gimplify.cc (gimplify_call_expr): Add error for multiple
	list items to the OpenMP interop clause if no device clause;
	continue instead of restarting after append_args handling.
	(gimplify_omp_dispatch): Extract device number from the
	single interop-clause list item.
	* omp-builtins.def (BUILT_IN_OMP_GET_INTEROP_INT): Add.

2025-01-08  Thomas Schwinge  <tschwinge@baylibre.com>

	PR target/65181
	* config/nvptx/nvptx.cc (default_ptx_version_option): For
	'-march=sm_52' and higher, default at least to '-mptx=7.3'.
	* doc/invoke.texi (Nvidia PTX Options): Update '-mptx=[...]'.

2025-01-08  Thomas Schwinge  <tschwinge@baylibre.com>

	* config/nvptx/nvptx-opts.h (enum ptx_version): Add
	'PTX_VERSION_7_3'.
	* config/nvptx/nvptx.cc (ptx_version_to_string)
	(ptx_version_to_number): Adjust.
	* config/nvptx/nvptx.h (TARGET_PTX_7_3): New.
	* config/nvptx/nvptx.opt (Enum(ptx_version)): Add 'EnumValue'
	'7.3' for 'PTX_VERSION_7_3'.
	* doc/invoke.texi (Nvidia PTX Options): Document '-mptx=7.3'.

2025-01-08  Thomas Schwinge  <tschwinge@baylibre.com>

	* doc/sourcebuild.texi (Effective-Target Keywords): Document
	'nvptx_softstack'.

2025-01-08  Thomas Schwinge  <tschwinge@baylibre.com>

	PR target/65181
	* config/nvptx/nvptx.h (STACK_SAVEAREA_MODE): '#define'.
	* config/nvptx/nvptx.md [!TARGET_SOFT_STACK]
	(save_stack_function): 'define_expand'.
	(restore_stack_function): Handle '!TARGET_SOFT_STACK'.

2025-01-08  Thomas Schwinge  <tschwinge@baylibre.com>

	PR target/65181
	* config/nvptx/nvptx.md [!TARGET_SOFT_STACK] (save_stack_block):
	'define_expand'.

2025-01-08  Thiago Jung Bauermann  <thiago.bauermann@linaro.org>

	* configure.ac: Fix check for HAVE_GAS_SHF_MERGE on Arm targets.
	* configure: Regenerate.

2025-01-08  Richard Sandiford  <richard.sandiford@arm.com>

	PR target/107102
	* config/aarch64/aarch64.cc (aarch64_function_ok_for_sibcall): Only
	reject calls with different PCSes if the callee clobbers register
	state that the caller must preserve.

2025-01-08  Tobias Burnus  <tburnus@baylibre.com>

	* gimplify.cc (gimplify_call_expr): Disable variant function's
	append_args in 'omp dispatch' when invoking the variant directly
	and not through the base function.

2025-01-08  Thomas Schwinge  <tschwinge@baylibre.com>

	* doc/invoke.texi (Nvidia PTX Options): Update '-march-map=sm_50'.

2025-01-08  Richard Biener  <rguenther@suse.de>

	PR tree-optimization/117979
	* tree-ssa-dce.cc (make_forwarders_with_degenerate_phis):
	Properly update the irreducible region state.

2025-01-08  Jakub Jelinek  <jakub@redhat.com>

	* dwarf2out.cc (break_out_comdat_types): Copy over
	DW_AT_language_{name,version} if present.
	(output_skeleton_debug_sections): Remove also
	DW_AT_language_{name,version}.
	(gen_compile_unit_die): For C17, C23, C2Y, C++17, C++20, C++23
	and C++26 emit for -gdwarf-5 -gno-strict-dwarf also
	DW_AT_language_{name,version} attributes.

2025-01-08  Richard Biener  <rguenther@suse.de>

	PR middle-end/118325
	* tree-nested.cc (convert_nl_goto_reference): Assign proper
	context to generated artificial label.

2025-01-08  Richard Biener  <rguenther@suse.de>

	PR tree-optimization/118269
	* tree-vect-loop.cc (vect_create_epilog_for_reduction):
	Use the correct stmt for the REDUC_GROUP_FIRST_ELEMENT lookup.

2025-01-08  Christophe Lyon  <christophe.lyon@linaro.org>

	PR target/118332
	* config/arm/arm-mve-builtins.cc (wrap_type_in_struct): Use 'val'
	instead of '__val'.

2025-01-08  Haochen Jiang  <haochen.jiang@intel.com>

	* config/i386/amxavx512intrin.h
	(_tile_cvtrowps2pbf16h_internal): Rename to...
	(_tile_cvtrowps2bf16h_internal): ...this.
	(_tile_cvtrowps2pbf16hi_internal): Rename to...
	(_tile_cvtrowps2bf16hi_internal): ...this.
	(_tile_cvtrowps2pbf16l_internal): Rename to...
	(_tile_cvtrowps2bf16l_internal): ...this.
	(_tile_cvtrowps2pbf16li_internal): Rename to...
	(_tile_cvtrowps2bf16li_internal): ...this.
	(_tile_cvtrowps2pbf16h): Rename to...
	(_tile_cvtrowps2bf16h): ...this.
	(_tile_cvtrowps2pbf16hi): Rename to...
	(_tile_cvtrowps2bf16hi): ...this.
	(_tile_cvtrowps2pbf16l): Rename to...
	(_tile_cvtrowps2bf16l): ...this.
	(_tile_cvtrowps2pbf16li): Rename to...
	(_tile_cvtrowps2bf16li): ...this.

2025-01-08  Hongyu Wang  <hongyu.wang@intel.com>

	* config/i386/i386.cc (ix86_noce_max_ifcvt_seq_cost): Adjust
	cost with ix86_tune_cost->br_mispredict_scale.
	* config/i386/i386.h (processor_costs): Add br_mispredict_scale.
	* config/i386/x86-tune-costs.h: Add new br_mispredict_scale to
	all processor_costs, in which icelake_cost/alderlake_cost
	with value COSTS_N_INSNS (2) + 3 and other processor with value
	COSTS_N_INSNS (2).

2025-01-07  Pan Li  <pan2.li@intel.com>

	* match.pd: Update comments for sat_* pattern.

2025-01-07  Pan Li  <pan2.li@intel.com>

	* match.pd: Extract saturated value match for signed SAT_*.

2025-01-07  Pan Li  <pan2.li@intel.com>

	* match.pd: Refactor sorts of signed SAT_TRUNC match patterns

2025-01-07  Pan Li  <pan2.li@intel.com>

	* match.pd: Refactor sorts of signed SAT_SUB match patterns.

2025-01-07  Vineet Gupta  <vineetg@rivosinc.com>
	    Pan Li  <pan2.li@intel.com>

	PR target/117722
	* config/riscv/autovec.md: Add uabd expander.

2025-01-07  Tsung Chun Lin  <tclin914@gmail.com>

	* expr.cc (widest_fixed_size_mode_for_size): Prefer scalar modes
	over vector modes in more cases.

2025-01-07  Andreas Schwab  <schwab@suse.de>

	PR target/118137
	* config/riscv/sync.md ("lrsc_atomic_exchange<mode>"): Apply mask
	to shifted value.

2025-01-07  Jeff Law  <jlaw@ventanamicro.com>

	* config/ft32/ft32.md (casesi expander): Force operands[2] into
	a register if it's not a suitable rimm operand.

2025-01-07  Wilco Dijkstra  <wilco.dijkstra@arm.com>

	* common/config/aarch64/aarch64-common.cc: Switch off fschedule_insns.

2025-01-07  Wilco Dijkstra  <wilco.dijkstra@arm.com>

	* config/aarch64/aarch64.md (movhf_aarch64): Use aarch64_valid_fp_move.
	(movsf_aarch64): Likewise.
	(movdf_aarch64): Likewise.
	* config/aarch64/aarch64.cc (aarch64_valid_fp_move): New function.
	* config/aarch64/aarch64-protos.h (aarch64_valid_fp_move): Likewise.

2025-01-07  Paul-Antoine Arras  <parras@baylibre.com>

	* gimplify.cc (gimplify_call_expr): Create variable
	variant_substituted_p to control whether adjust_args applies.

2025-01-07  Tamar Christina  <tamar.christina@arm.com>

	PR tree-optimization/114932
	* tree-ssa-loop-ivopts.cc (alloc_iv): Perform affine unsigned fold.

2025-01-07  Andrew Pinski  <quic_apinski@quicinc.com>

	PR tree-optimization/105769
	* cfgexpand.cc (vars_ssa_cache::operator()): For constructors
	walk over the elements.

2025-01-07  Andrew Pinski  <quic_apinski@quicinc.com>

	PR middle-end/117426
	PR middle-end/111422
	* cfgexpand.cc (struct vars_ssa_cache): New class.
	(vars_ssa_cache::vars_ssa_cache): New constructor.
	(vars_ssa_cache::~vars_ssa_cache): New deconstructor.
	(vars_ssa_cache::create): New method.
	(vars_ssa_cache::exists): New method.
	(vars_ssa_cache::add_one): New method.
	(vars_ssa_cache::update): New method.
	(vars_ssa_cache::dump): New method.
	(add_scope_conflicts_2): Factor mostly out to
	vars_ssa_cache::operator(). New cache argument.
	Walk the bitmap cache for the stack variables addresses.
	(vars_ssa_cache::operator()): New method factored out from
	add_scope_conflicts_2. Rewrite to be a full walk of all operands
	and use a worklist.
	(add_scope_conflicts_1): Add cache new argument for the addr cache.
	Just call add_scope_conflicts_2 for the phi result instead of calling
	for the uses and don't call walk_stmt_load_store_addr_ops for phis.
	Update call to add_scope_conflicts_2 to add cache argument.
	(add_scope_conflicts): Add cache argument and update calls to
	add_scope_conflicts_1.

2025-01-07  Andrew Pinski  <quic_apinski@quicinc.com>

	* cfgexpand.cc (INVALID_STACK_INDEX): New defined.
	(decl_stack_index): New function.
	(visit_op): Use decl_stack_index.
	(visit_conflict): Likewise.
	(add_scope_conflicts_1): Likewise.

2025-01-07  Richard Biener  <rguenther@suse.de>

	PR rtl-optimization/118298
	* loop-unroll.cc (decide_unroll_constant_iterations): Honor
	loop->unroll even if the loop is too big for heuristics.

2025-01-07  Deng Jianbo  <dengjianbo@loongson.cn>

	* config/loongarch/loongarch.cc (loongarch_output_move):
	Optimize instructions for initializing fp regsiter to zero.

2025-01-07  Gaius Mulley  <gaiusmod2@gmail.com>

	PR modula2/118010
	* doc/gm2.texi (Compiler options): New option
	-fm2-file-offset-bits=.

2025-01-07  Jennifer Schmitz  <jschmitz@nvidia.com>

	* tree-vect-stmts.cc (vectorizable_store): Extend the use of
	n_adjacent_stores to also cover vec_to_scalar operations.
	* config/aarch64/aarch64-tuning-flags.def: Remove
	use_new_vector_costs as tuning option.
	* config/aarch64/aarch64.cc (aarch64_use_new_vector_costs_p):
	Remove.
	(aarch64_vector_costs::add_stmt_cost): Remove use of
	aarch64_use_new_vector_costs_p.
	(aarch64_vector_costs::finish_cost): Remove use of
	aarch64_use_new_vector_costs_p.
	* config/aarch64/tuning_models/cortexx925.h: Remove
	AARCH64_EXTRA_TUNE_USE_NEW_VECTOR_COSTS.
	* config/aarch64/tuning_models/fujitsu_monaka.h: Likewise.
	* config/aarch64/tuning_models/generic_armv8_a.h: Likewise.
	* config/aarch64/tuning_models/generic_armv9_a.h: Likewise.
	* config/aarch64/tuning_models/neoverse512tvb.h: Likewise.
	* config/aarch64/tuning_models/neoversen2.h: Likewise.
	* config/aarch64/tuning_models/neoversen3.h: Likewise.
	* config/aarch64/tuning_models/neoversev1.h: Likewise.
	* config/aarch64/tuning_models/neoversev2.h: Likewise.
	* config/aarch64/tuning_models/neoversev3.h: Likewise.
	* config/aarch64/tuning_models/neoversev3ae.h: Likewise.

2025-01-06  Alexandre Oliva  <oliva@adacore.com>

	PR middle-end/118006
	* cfgexpand.cc (expand_gimple_basic_block): Do not emit
	pending stack adjustments after a barrier.

2025-01-06  Akram Ahmad  <Akram.Ahmad@arm.com>

	* config/aarch64/aarch64-simd.md: (*aarch64_trunc_concat)
	new insn definition.

2025-01-06  Fangrui Song  <maskray@gcc.gnu.org>

	PR gcov-profile/96092
	* coverage.cc (coverage_init): Remap getpwd().

2025-01-06  Jennifer Schmitz  <jschmitz@nvidia.com>

	* config/aarch64/aarch64-sve-builtins-base.cc
	(svmul_impl::fold): Wrap code for folding to svneg in lambda
	function and pass to gimple_folder::convert_and_fold to enable
	the transform for unsigned types.
	* config/aarch64/aarch64-sve-builtins.cc
	(gimple_folder::convert_and_fold): New function that converts
	operands to target type before calling callback function, adding the
	necessary conversion statements.
	(gimple_folder::redirect_call): Set fntype of redirected call.
	(get_vector_type): Move from here to aarch64-sve-builtins.h.
	* config/aarch64/aarch64-sve-builtins.h
	(gimple_folder::convert_and_fold): Declare function.
	(get_vector_type): Move here as inline function.

2025-01-06  Martin Jambor  <mjambor@suse.cz>

	* ipa-cp.cc (ipcp_print_widest_int): New function.
	(ipcp_store_vr_results): Use it.
	(ipcp_bits_lattice::print): Likewise.  Fix formatting.

2025-01-06  Mark Wielaard  <mark@klomp.org>

	PR tree-optimization/118032
	* tree-switch-conversion.cc (jump_table_cluster::find_jump_tables):
	Remove param_switch_lower_slow_alg_max_cases check.

2025-01-06  Tamar Christina  <tamar.christina@arm.com>

	PR target/96342
	PR target/118272
	* config/aarch64/aarch64-sve.md (vec_init<mode><Vquad>,
	vec_initvnx16qivnx2qi): New.
	* config/aarch64/aarch64.cc (aarch64_sve_expand_vector_init_subvector):
	Rewrite to support any arbitrary combinations.
	* config/aarch64/iterators.md (SVE_NO2E): Update to use SVE_NO4E
	(SVE_NO2E, Vquad): New.

2025-01-06  Jakub Jelinek  <jakub@redhat.com>

	PR tree-optimization/118224
	* tree-ssa-dce.cc (is_removable_allocation_p): Don't return true
	for allocations with constant size argument larger than PTRDIFF_MAX
	or for calloc with one of the arguments constant larger than
	PTRDIFF_MAX or their product known constant above PTRDIFF_MAX.
	Fix comment typos, furhter -> further and then -> than.
	* lto-section-in.cc (lto_free_function_in_decl_state_for_node):
	Fix comment typo, furhter -> further.

2025-01-04  Hans-Peter Nilsson  <hp@axis.com>

	* config/mmix/mmix.cc (mmix_asm_output_labelref): Replace '.'
	with '::'.
	* config/mmix/mmix.h (ASM_PN_FORMAT): Define to actual default.

2025-01-03  Richard Sandiford  <richard.sandiford@arm.com>

	PR rtl-optimization/117938
	* rtlanal.cc (rtx_properties::try_to_add_dest): Treat writes
	to the stack pointer as also writing to memory.

2025-01-03  Jakub Jelinek  <jakub@redhat.com>

	PR c++/118275
	* varasm.cc (array_size_for_constructor): Use build_int_cst
	with TREE_TYPE (index) as first argument, instead of bitsize_int.

2025-01-03  Jakub Jelinek  <jakub@redhat.com>

	* tree-ssa-forwprop.cc (check_ctz_array): Use tree_fits_shwi_p instead
	of just TREE_CODE tests for INTEGER_CST.

2025-01-03  Jose E. Marchesi  <jose.marchesi@oracle.com>

	* config.gcc: install a wrapping stdint.h in bpf targets.

2025-01-02  Paul-Antoine Arras  <parras@baylibre.com>

	* gimplify.cc (gimplify_call_expr): Fix handling of need_device_ptr for
	type(c_ptr). Fix handling of nested function calls in a dispatch region.
	(find_ifn_gomp_dispatch): Return the IFN without stripping it.
	(gimplify_omp_dispatch): Keep IFN_GOMP_DISPATCH until
	gimplify_call_expr.

2025-01-02  Tobias Burnus  <tburnus@baylibre.com>

	* doc/install.texi (amdgcn-x-amdhsa): Refer to Newlib 4.5.0 for
	the I/O locking fixes.

2025-01-02  Richard Biener  <rguenther@suse.de>

	PR tree-optimization/118171
	* tree-ssa-pre.cc (create_component_ref_by_pieces_1): Do not
	fold any component ref parts.

2025-01-02  Richard Sandiford  <richard.sandiford@arm.com>

	PR target/118184
	* config/aarch64/aarch64-early-ra.cc (allocno_assignment_is_rmw):
	New function.
	(early_ra::record_insn_defs): Mark the live range information as
	untrustworthy if an assignment would change part of an allocno
	but preserve the rest.

2025-01-02  Jakub Jelinek  <jakub@redhat.com>

	* tree-ssa-forwprop.cc (check_ctz_array): Handle also RAW_DATA_CST
	in the CONSTRUCTOR_ELTS.

2025-01-02  Jakub Jelinek  <jakub@redhat.com>

	* doc/libgdiagnostics/conf.py: Use u'' instead of '' in
	project and copyright initialization.

2025-01-02  Jakub Jelinek  <jakub@redhat.com>

	* gcc.cc (process_command): Update copyright notice dates.
	* gcov-dump.cc (print_version): Ditto.
	* gcov.cc (print_version): Ditto.
	* gcov-tool.cc (print_version): Ditto.
	* gengtype.cc (create_file): Ditto.
	* doc/cpp.texi: Bump @copying's copyright year.
	* doc/cppinternals.texi: Ditto.
	* doc/gcc.texi: Ditto.
	* doc/gccint.texi: Ditto.
	* doc/gcov.texi: Ditto.
	* doc/install.texi: Ditto.
	* doc/invoke.texi: Ditto.

2025-01-02  Guo Jie  <guojie@loongson.cn>

	* config/loongarch/loongarch.cc
	(loongarch_expand_conditional_move): Add some optimization
	implementations based on noce_try_cmove_arith.

2025-01-02  Guo Jie  <guojie@loongson.cn>

	* config/loongarch/lasx.md (lasx_xvabsd_s_<lasxfmt>): Remove.
	(<su>abd<mode>3): New insn pattern.
	(lasx_xvabsd_u_<lasxfmt_u>): Remove.
	* config/loongarch/loongarch-builtins.cc (CODE_FOR_lsx_vabsd_b):
	Rename.
	(CODE_FOR_lsx_vabsd_h): Ditto.
	(CODE_FOR_lsx_vabsd_w): Ditto.
	(CODE_FOR_lsx_vabsd_d): Ditto.
	(CODE_FOR_lsx_vabsd_bu): Ditto.
	(CODE_FOR_lsx_vabsd_hu): Ditto.
	(CODE_FOR_lsx_vabsd_wu): Ditto.
	(CODE_FOR_lsx_vabsd_du): Ditto.
	(CODE_FOR_lasx_xvabsd_b): Ditto.
	(CODE_FOR_lasx_xvabsd_h): Ditto.
	(CODE_FOR_lasx_xvabsd_w): Ditto.
	(CODE_FOR_lasx_xvabsd_d): Ditto.
	(CODE_FOR_lasx_xvabsd_bu): Ditto.
	(CODE_FOR_lasx_xvabsd_hu): Ditto.
	(CODE_FOR_lasx_xvabsd_wu): Ditto.
	(CODE_FOR_lasx_xvabsd_du): Ditto.
	* config/loongarch/loongarch.md (u): Add smax/umax.
	* config/loongarch/lsx.md (SU_MAX): New iterator.
	(su_min): New attr.
	(lsx_vabsd_s_<lsxfmt>): Remove.
	(<su>abd<mode>3): New insn pattern.
	(lsx_vabsd_u_<lsxfmt_u>): Remove.

2025-01-02  Guo Jie  <guojie@loongson.cn>

	* config/loongarch/lasx.md (vec_unpacks_lo_<mode>): Redefine.
	(vec_unpacku_lo_<mode>): Ditto.
	(lasx_vext2xv_h<u>_b<u>): Replaced by vec_unpack<su>_lo_v32qi.
	(vec_unpack<su>_lo_v32qi): New insn.
	(lasx_vext2xv_w<u>_h<u>): Replaced by vec_unpack<su>_lo_v16hi.
	(vec_unpack<su>_lo_v16qi_internal): New insn, for 128 bits.
	(vec_unpack<su>_lo_v16hi): New insn.
	(lasx_vext2xv_d<u>_w<u>): Replaced by vec_unpack<su>_lo_v8si.
	(vec_unpack<su>_lo_v8hi_internal): New insn, for 128 bits.
	(vec_unpack<su>_lo_v8si): New insn.
	(vec_unpack<su>_lo_v4si_internal): New insn, for 128 bits.
	(vec_packs_float_v4di): New expander.
	(vec_pack_sfix_trunc_v4df): Ditto.
	(vec_unpacks_float_hi_v8si): Ditto.
	(vec_unpacks_float_lo_v8si): Ditto.
	(vec_unpack_sfix_trunc_hi_v8sf): Ditto.
	(vec_unpack_sfix_trunc_lo_v8sf): Ditto.
	* config/loongarch/loongarch-builtins.cc
	(CODE_FOR_lsx_vftintrz_w_d): Rename.
	(CODE_FOR_lsx_vftintrzh_l_s): Ditto.
	(CODE_FOR_lsx_vftintrzl_l_s): Ditto.
	(CODE_FOR_lsx_vffint_s_l): Ditto.
	(CODE_FOR_lsx_vffinth_d_w): Ditto.
	(CODE_FOR_lsx_vffintl_d_w): Ditto.
	(CODE_FOR_lsx_vexth_h_b): Ditto.
	(CODE_FOR_lsx_vexth_w_h): Ditto.
	(CODE_FOR_lsx_vexth_d_w): Ditto.
	(CODE_FOR_lsx_vexth_hu_bu): Ditto.
	(CODE_FOR_lsx_vexth_wu_hu): Ditto.
	(CODE_FOR_lsx_vexth_du_wu): Ditto.
	(CODE_FOR_lsx_vfcvth_d_s): Ditto.
	(CODE_FOR_lsx_vfcvtl_d_s): Ditto.
	(CODE_FOR_lasx_vext2xv_h_b): Ditto.
	(CODE_FOR_lasx_vext2xv_w_h): Ditto.
	(CODE_FOR_lasx_vext2xv_d_w): Ditto.
	(CODE_FOR_lasx_vext2xv_hu_bu): Ditto.
	(CODE_FOR_lasx_vext2xv_wu_hu): Ditto.
	(CODE_FOR_lasx_vext2xv_du_wu): Ditto.
	(loongarch_expand_builtin_insn): Swap source operands in
	CODE_FOR_lsx_vftintrz_w_d and CODE_FOR_lsx_vffint_s_l.
	* config/loongarch/loongarch-protos.h
	(loongarch_expand_vec_unpack): Remove useless parameter high_p.
	* config/loongarch/loongarch.cc (loongarch_expand_vec_unpack):
	Rewrite.
	* config/loongarch/lsx.md (vec_unpacks_hi_v4sf): Redefine.
	(vec_unpacks_lo_v4sf): Ditto.
	(vec_unpacks_hi_<mode>): Ditto.
	(vec_unpacku_hi_<mode>): Ditto.
	(lsx_vfcvth_d_s): Replaced by vec_unpacks_hi_v4sf.
	(lsx_vfcvtl_d_s): Replaced by vec_unpacks_lo_v4sf.
	(lsx_vffint_s_l): Replaced by vec_packs_float_v2di.
	(vec_packs_float_v2di): New insn.
	(lsx_vftintrz_w_d): Replaced by vec_pack_sfix_trunc_v2df.
	(vec_pack_sfix_trunc_v2df): New insn.
	(lsx_vffinth_d_w): Replaced by vec_unpacks_float_hi_v4si.
	(vec_unpacks_float_hi_v4si): New insn.
	(lsx_vffintl_d_w): Replaced by vec_unpacks_float_lo_v4si.
	(vec_unpacks_float_lo_v4si): New insn.
	(lsx_vftintrzh_l_s): Replaced by vec_unpack_sfix_trunc_hi_v4sf.
	(vec_unpack_sfix_trunc_hi_v4sf): New insn.
	(lsx_vftintrzl_l_s): Replaced by vec_unpack_sfix_trunc_lo_v4sf.
	(vec_unpack_sfix_trunc_lo_v4sf): New insn.
	(lsx_vexth_h<u>_b<u>): Replaced by vec_unpack<su>_hi_v16qi.
	(vec_unpack<su>_hi_v16qi): New insn.
	(lsx_vexth_w<u>_h<u>): Replaced by vec_unpack<su>_hi_v8hi.
	(vec_unpack<su>_hi_v8hi): New insn.
	(lsx_vexth_d<u>_w<u>): Replaced by vec_unpack<su>_hi_v4si.
	(vec_unpack<su>_hi_v4si): New insn.

2025-01-02  Guo Jie  <guojie@loongson.cn>

	* config/loongarch/loongarch.md
	(bytepick_d_<bytepick_imm>_rev): New combiner.
	(bstrpick_alsl_paired): Reorder input operands.

2025-01-02  Guo Jie  <guojie@loongson.cn>

	* config/loongarch/lasx.md: Remove useless vec_select.
	* config/loongarch/predicates.md: Correct error predicate.

2025-01-02  Guo Jie  <guojie@loongson.cn>

	* config/loongarch/lasx.md: Fix selector index.

2025-01-02  Guo Jie  <guojie@loongson.cn>

	* config/loongarch/lasx.md: Remove useless code.
	* config/loongarch/lsx.md: Ditto.

2025-01-01  Sam James  <sam@gentoo.org>

	* doc/cpp.texi (Common Predefined Macros): Fix syntax.

2025-01-01  Richard Biener  <rguenther@suse.de>

	PR middle-end/118174
	* tree-outof-ssa.cc (ssa_is_replaceable_p): Exclude tailcalls.

2025-01-01  Sandra Loosemore  <sloosemore@baylibre.com>

	* doc/invoke.texi (Option Summary): Put "M32C Options" and
	"Cygwin and MinGW Options" in alphabetical order.  Add
	cross-references.
	(Cygwin and MinGW Options): Likewise move the section to its
	correct alphabetical location.
	* config/lynx.opt.urls: Regenerated.
	* config/mingw/cygming.opt.urls: Regenerated.

Copyright (C) 2025 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.
